import os
import xml.etree.ElementTree as ET
import pickle
from collections import defaultdict, Counter
from datetime import datetime
from openpyxl import Workbook

import pygal
import enchant
from pygal.style import LightColorizedStyle


class Post:
    def __init__(self, xmlnode, time):
        self.author = xmlnode[0][0].text
        self.time = time
        self.sentences = ["".join(sentence.itertext()).strip() for sentence in
                          (xmlnode.iter("{http://www.tei-c.org/ns/1.0}s"))]
        self.firsts = [sentence.split(" ")[0] for sentence in self.sentences]
        self.emoticons = [gap.attrib["rend"].replace("_emotikon", "").replace("emotikon ", "").lower() for gap in
                          xmlnode[2].iter("{http://www.tei-c.org/ns/1.0}gap") if
                          "rend" in gap.attrib and gap.attrib["rend"] != "emotikon"]

    def __repr__(self):
        return "---\n" \
               "autor: '{}'\n" \
               "aeg: '{}'\n" \
               "laused: '{}'\n" \
               "lause esimesed sõnad: '{}'\n" \
               "emotikonid: '{}'\n\n".format(self.author,
                                             self.time,
                                             self.sentences,
                                             self.firsts,
                                             self.emoticons)


class Data:
    # --- Begin data initialization ---

    def __init__(self):

        self.posts_by_year = {}
        self.posts_by_author = {}
        self.posts_by_year_regular = {}
        self.posts_by_author_regular = {}
        self.posts_by_year_rest = {}
        self.posts_by_author_rest = {}
        self.all_years = list(range(2001, 2009))
        self.all_emoticons = set()
        self.regular_users = (
            "troskal", "treudenbauer", "naine", "Changer", "giga",
            "anarchy", "manic", "peekaa", "jahimees", "Max",
            "shk", "Kyra", "Erx007", "Killer85", "Levis",
            "Muuc", "Arnel", "tsadeh", "Kristo Vaher", "DMX",
            "Pontz", "Conwerz", "midfield", "zimm", "MoX",
            "R-mees", "Lord Nicon", "-CH-swompy", "Funkyzone", "Dark",
            "glaurung", "Tha Killah", "BigBabba", "krustok", "Lumeinimene",
            "Brent", "Sardokaur", "zeus", "alex", "sulik",
            "kireE", "Q3ah",
        )

        self.author_counts = None
        self.special_letters = ('c', 'q', 'w', 'x', 'y')
        self.post_counts_series = []
        self.special_letter_word_counters = []
        self.lengthened_word_counters = []
        self.est_abbreviation_counters = []
        self.eng_abbreviation_counters = []

        for filename in os.listdir("foorumid/"):
            tree = ET.parse("foorumid/" + filename)
            for postnode in tree.iter("{http://www.tei-c.org/ns/1.0}div3"):
                try:
                    post = Post(postnode, datetime.strptime(postnode[1][0].text.strip(), "%d-%m-%Y , %H:%M"))
                except ValueError:
                    # Some posts are missing their date (datetime.strptime will fail),
                    # these posts are not useful to us so we will ignore them
                    continue

                self.all_emoticons.update(post.emoticons)
                year = post.time.year
                self.add_post(post, year)

        eng_d = enchant.Dict("en_US")
        est_d = enchant.Dict("et_EE")

        ignored_special_words = (
            "xp", "cd", "windows", "windowsi", "linux", "linuxi", "geforce", "sony", "toyota", "offtopic",
            "firefox", "pc", "bmw", "2x", "sry", "wtf", "btw", "celeron", "winamp", "wifi", "mysql",
            "microsoft"
        )

        ignored_lengthened_words = ("iii", "piii", "kkk", "bbbb")

        ignored_abbreviations = ("aaaa", "kyll", "nyyd", "peaaegu", "mysql", "yldse", "hooaeg", "html", "system")

        eng_abbrevations = (
            "lol", "lmfao", "lmao", "plz", "rofl", "wtf", "brb", "g2g", "sry", "imo", "imho", "btw", "ftw", "omg")

        for year, posts in self.posts_by_year.items():

            print(year)

            self.post_counts_series.append(len(posts))

            special_letter_year_counter = Counter()
            lengthened_word_year_counter = Counter()
            est_abbreviation_year_counter = Counter()
            eng_abbreviation_year_counter = Counter()

            for i, post in enumerate(posts):
                if i % 5000 == 0:
                    print("year: {}, progress: {}, len: {}".format(year, i, len(posts)))
                for sentence in post.sentences:
                    special_letter_year_counter += Counter(
                        word.lower() for word in sentence.split(" ") if
                        word.lower() not in ignored_special_words and
                        any(special_letter in word.lower() for special_letter in self.special_letters) and
                        not eng_d.check(word.lower())
                    )

                    lengthened_word_year_counter += Counter(
                        word.lower() for word in sentence.split(" ") if word.lower() not in ignored_lengthened_words and
                        self.has_consecutive_letters(word.lower()) and
                        not est_d.check(word.lower())
                    )

                    est_abbreviation_year_counter += Counter(
                        word.lower() for word in sentence.split(" ") if word.lower() not in ignored_abbreviations and
                        self.is_abbreviation(word.lower())
                    )

                    eng_abbreviation_year_counter += Counter(
                        word.lower() for word in sentence.split(" ") if word.lower() in eng_abbrevations
                    )

            self.special_letter_word_counters.append(special_letter_year_counter)
            self.lengthened_word_counters.append(lengthened_word_year_counter)
            self.est_abbreviation_counters.append(est_abbreviation_year_counter)
            self.eng_abbreviation_counters.append(eng_abbreviation_year_counter)

    def has_consecutive_letters(self, word):
        previous_letter = None
        counter = 0
        for letter in word:
            if letter.isalpha() and previous_letter == letter:
                counter += 1
                if counter == 2:
                    return True
            else:
                counter = 0
                previous_letter = letter
        return False

    def is_abbreviation(self, word):

        abbreviations = (
            "krt", "ptv", "ntx", "tre", "pmst", "sry", "dvi", "dvj", "dav", "nv", "mhm", "mkm", "mhmh", "mkmk", "tglt",
            "rsk"
        )

        return word in abbreviations or self.has_consecutive_consonants_or_vowels(word)

    def has_consecutive_consonants_or_vowels(self, word):

        cons = ('b', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'r', 's', 'š', 'ž', 't', 'v', 'w', 'y', 'q', 'x')

        vows = ('a', 'e', 'i', 'o', 'u', 'õ', 'ä', 'ö', 'ü')

        previous_letter = None
        counter = 0
        for letter in word:
            if previous_letter in cons and letter in cons or previous_letter in vows and letter in vows:
                counter += 1
                if counter == 3:
                    return True
            else:
                counter = 0
                previous_letter = letter
        return False

    def add_post(self, post, year):
        self.add_post_to_dicts(post, year, self.posts_by_year, self.posts_by_author)
        if post.author in self.regular_users:
            self.add_post_to_dicts(post, year, self.posts_by_year_regular, self.posts_by_author_regular)
        else:
            self.add_post_to_dicts(post, year, self.posts_by_year_rest, self.posts_by_author_rest)

    def add_post_to_dicts(self, post, year, by_year_dict, by_author_dict):
        if year not in by_year_dict:
            by_year_dict[year] = [post]
        else:
            by_year_dict[year].append(post)
        if post.author not in by_author_dict:
            by_author_dict[post.author] = {year: [post]}
        else:
            if year not in by_author_dict[post.author]:
                by_author_dict[post.author][year] = [post]
            else:
                by_author_dict[post.author][year].append(post)

    # --- End data initialization ---

    # --- Begin utility methods ---

    def draw_graphs(self):
        for graph_method in (getattr(self, method_name) for method_name in dir(self) if method_name[:6] == "graph_"):
            graph_method()

    def calculate_indexes(self, *args):
        result = []
        for series in args:
            first_nonzero_index = 0
            for index, element in enumerate(series):
                if element != 0:
                    first_nonzero_index = index
                    break
            result.append([None for _ in range(first_nonzero_index)] + [count / series[first_nonzero_index] for count in
                                                                        series[first_nonzero_index:]])
        if len(result) == 1:
            return result[0]
        else:
            return result

    def get_author_counts_by_year(self):
        if self.author_counts:
            return self.author_counts
        result = []
        for year, posts in self.posts_by_year.items():
            authors = set()
            for post in posts:
                authors.add(post.author)
            result.append(len(authors))
        self.author_counts = result
        return result

    # --- End utility methods ---

    # --- Begin emoticon graphs ---

    def get_specific_emoticon_frequencies(self):
        result = {}
        for year, posts in self.posts_by_year.items():
            result[year] = Counter(emoticon for post in posts for emoticon in post.emoticons)
        return result

    def graph_emoticons_most_used(self):
        top_emoticons = {}
        emoticon_frequencies = self.get_specific_emoticon_frequencies()
        for year, counter in emoticon_frequencies.items():
            for emoticon, count in counter.items():
                counter[emoticon] = (count / self.post_counts_series[year - 2001]) * 100
        for year, counter in emoticon_frequencies.items():
            for emoticon, count in counter.most_common(3):
                top_emoticons[emoticon] = []
        for year, counter in emoticon_frequencies.items():
            for emoticon in top_emoticons.keys():
                top_emoticons[emoticon].append(counter[emoticon])
        chart = pygal.Line(legend_at_bottom=True, style=LightColorizedStyle)
        chart.title = "Populaarseimate emotikonide kasutus aastas (100 postituse kohta)"
        chart.x_labels = map(str, range(2001, 2009))
        for emoticon, series in top_emoticons.items():
            chart.add(emoticon, series)
        chart.render_to_png("graphs/emotikonid_enimkasutatud.png")

    def graph_emoticons_vs_unique_users(self):
        author_counts = self.get_author_counts_by_year()

        emoticon_counts_all = self.get_emoticon_series(self.posts_by_year)

        print("emoticon_counts_all: {}".format(emoticon_counts_all))
        print("author_counts: {}".format(author_counts))

        emoticon_indexes, author_indexes = self.calculate_indexes(emoticon_counts_all, author_counts)

        chart = pygal.Line(legend_at_bottom=True, style=LightColorizedStyle)
        chart.title = "Emotikonide kasutus võrreldes kasutajate arvuga"
        chart.x_labels = map(str, range(2001, 2009))

        chart.add("Emotikonide/postituste suhte indeks", emoticon_indexes)
        chart.add("Erinevate postitajate indeks", author_indexes)
        chart.render_to_png("graphs/emotikonid_vs_kasutajad.png")

    def graph_emoticons_regular_vs_rest(self):
        emoticon_counts_regular = self.get_emoticon_series(self.posts_by_year_regular)
        emoticon_counts_rest = self.get_emoticon_series(self.posts_by_year_rest)

        print("emoticon_counts_regular: {}".format(emoticon_counts_regular))
        print("emoticon_counts_rest: {}".format(emoticon_counts_rest))

        emoticon_indexes_regular, emoticon_indexes_rest = self.calculate_indexes(emoticon_counts_regular,
                                                                                 emoticon_counts_rest)

        chart = pygal.Line(legend_at_bottom=True, style=LightColorizedStyle)
        chart.title = "Emotikonide/postituste suhte indeks"
        chart.x_labels = map(str, range(2001, 2009))

        chart.add("Püsikasutajad", emoticon_indexes_regular)
        chart.add("Tavakasutajad", emoticon_indexes_rest)
        chart.render_to_png("graphs/emotikonid_regular_vs_teised.png")

    def get_emoticon_series(self, posts_by_year):
        emoticon_counts = []
        for year, posts in posts_by_year.items():
            count = 0
            for post in posts:
                count += len(post.emoticons)
            emoticon_counts.append((count / len(posts)) * 100)
        return emoticon_counts

    # --- End emoticon graphs ---

    # --- Begin user graphs ---

    def graph_average_posts_per_user(self):

        average_series = [post_count / author_count for post_count, author_count in
                          zip(self.post_counts_series, self.get_author_counts_by_year())]

        chart = pygal.Line(legend_at_bottom=True, style=LightColorizedStyle)
        chart.title = "Keskmine postituste arv kasutaja kohta"
        chart.x_labels = map(str, range(2001, 2009))

        chart.add("Postituste arv", average_series)
        chart.render_to_png("graphs/kasutajad_keskmine_postituste_arv.png")

    def graph_users_by_post_count(self):

        regular_series = [author_count * 100 / len(self.posts_by_author_regular.keys()) for author_count in
                          self.get_post_count_range_series(self.posts_by_author_regular)]
        rest_series = [author_count * 100 / len(self.posts_by_author_rest.keys()) for author_count in
                       self.get_post_count_range_series(self.posts_by_author_rest)]

        line_chart = pygal.Bar(legend_at_bottom=True, style=LightColorizedStyle, x_label_rotation=40)
        line_chart.title = "Kasutajate jaotus postituste arvu järgi (protsentides)"
        line_chart.x_labels = list(map(str, range(1, 11))) + ["11 - 20", "21 - 30", "31 - 40", "41 - 50", "51 - 60",
                                                              "61 - 70", "71 - 80", "81 - 90", "91 - 100", "101 - 200",
                                                              "201 - 300", "301 - 400", "401 - 500", "501 - 1000",
                                                              "1001 - 1500", "1501 - 2000", "2001 - 2500",
                                                              "2501 - 3000", "3001 - 3500", "3501 - 4000",
                                                              "4001 - 4500", "4501 - 5000"]
        line_chart.add("Püsikasutajad", regular_series)
        line_chart.add("Tavakasutajad", rest_series)
        line_chart.render_to_png("graphs/kasutajad_postituste_arvud.png")

    def get_post_count_range_series(self, posts_by_author):
        result = [0 for _ in range(32)]
        for author, posts_by_year in posts_by_author.items():
            post_count = 0
            for posts in posts_by_year.values():
                post_count += len(posts)

            if post_count <= 10:
                result[post_count - 1] += 1
            elif post_count <= 100:
                result[9 + (post_count // 10)] += 1
            elif post_count <= 500:
                result[18 + (post_count // 100)] += 1
            else:
                result[22 + (post_count // 500)] += 1

        return result

    def graph_new_existing_users(self):

        new_authors_series = [0 for _ in range(8)]

        for author, posts_by_year in self.posts_by_author.items():
            for i in range(8):
                if i + 2001 in posts_by_year:
                    new_authors_series[i] += 1
                    break

        existing_authors_series = [author_count - new_author_count for author_count, new_author_count in
                                   zip(self.get_author_counts_by_year(), new_authors_series)]

        full_authors_series = [sum(new_authors_series[:i + 1]) - new_author_count - existing_author_count for
                               i, (new_author_count, existing_author_count) in
                               enumerate(zip(new_authors_series, existing_authors_series))]

        chart = pygal.StackedLine(legend_at_bottom=True, style=LightColorizedStyle, fill=True)
        chart.title = "Uued ja olemasolevad autorid"
        chart.x_labels = map(str, range(2001, 2009))

        print("existing_authors_series: {}".format(existing_authors_series))
        print("new_authors_series: {}".format(new_authors_series))

        chart.add("Vanad kasutajad", existing_authors_series)
        chart.add("Uued kasutajad", new_authors_series)
        chart.add("Kasutajate arv lahkunuid arvestamata", full_authors_series,
                  stroke_style={"dasharray": "3, 6"}, fill=False)

        chart.render_to_png("graphs/kasutajad_uued_vanad.png")

    def graph_last_post(self):

        last_post_authors_series = [0 for _ in range(8)]

        for author, posts_by_year in self.posts_by_author.items():
            for i in range(8):
                year = 2008 - i
                if year in posts_by_year:
                    if year != 2008:
                        last_post_authors_series[8 - i] += 1
                    break

        chart = pygal.Line(legend_at_bottom=True, style=LightColorizedStyle)
        chart.title = "Vaadeldaval perioodil lahkunud autorid"
        chart.x_labels = map(str, range(2001, 2009))

        print("last_post_authors_series: {}".format(last_post_authors_series))

        chart.add("Eelneval aastal viimase postituse teinud autorite arv", last_post_authors_series)

        chart.render_to_png("graphs/kasutajad_lahkunud.png")

    def graph_posts_words_distribution(self):

        regular_result = self.get_posts_words_data_points(self.posts_by_author_regular)
        rest_result = self.get_posts_words_data_points(self.posts_by_author_rest)

        chart = pygal.XY(legend_at_bottom=True, style=LightColorizedStyle, stroke=False, dots_size=2,
                         show_x_guides=True)
        chart.title = "Kasutajate jaotus postituste arvu ning sõnade arvu järgi"
        chart.add("Tavakasutajad", rest_result)
        chart.add("Püsikasutajad", regular_result)
        chart.render_to_png("graphs/kasutajad_postitused_sonad_jaotus.png")

    def get_posts_words_data_points(self, posts_by_author):
        result = []
        for author, posts_by_year in posts_by_author.items():

            post_count = 0
            word_count = 0

            for posts in posts_by_year.values():
                for post in posts:
                    post_count += 1
                    for sentence in post.sentences:
                        word_count += len(sentence.split(" "))
            if post_count > 4000 or word_count > 150000:
                print("kasutaja: {}, postitusi: {}, sõnu: {}".format(author, post_count, word_count))
            result.append((post_count, word_count))
        return result

    # --- End user graphs ---

    # --- Begin capitalization graphs ---


    def graph_capitalization(self):
        lower_users, upper_users = self.get_upper_lower_user_series(self.posts_by_author)
        chart = pygal.Line(legend_at_bottom=True, style=LightColorizedStyle)
        chart.title = "Suure ja väikese algustähe kasutajad aastas"
        chart.x_labels = map(str, range(2001, 2009))

        chart.add("Suure algustähe kasutajad", upper_users)
        chart.add("Väikese algustähe kasutajad", lower_users)
        chart.render_to_png("graphs/algust2hed.png")

    def graph_capitalization_regular_vs_rest(self):
        lower_users_regular, _ = self.get_upper_lower_user_series(self.posts_by_author_regular)
        lower_users_rest, _ = self.get_upper_lower_user_series(self.posts_by_author_rest)

        regular_percentages = [lower_user_count * 100 / len(self.regular_users) for lower_user_count in
                               lower_users_regular]
        rest_percentages = []
        for i, lower_user_count in enumerate(lower_users_rest):
            rest_percentages.append(
                lower_user_count * 100 / (self.get_author_counts_by_year()[i] - len(self.regular_users))
            )

        print("capitalization_regular_percentages: {}".format(regular_percentages))
        print("capitalization_rest_precetanges: {}".format(rest_percentages))

        regular_lower_indexes, rest_lower_indexes = self.calculate_indexes(regular_percentages, rest_percentages)

        chart = pygal.Line(legend_at_bottom=True, style=LightColorizedStyle)
        chart.title = "Väikese algustähe kasutus"
        chart.x_labels = map(str, range(2001, 2009))

        chart.add("Püsikasutajad", regular_lower_indexes)
        chart.add("Tavakasutajad", rest_lower_indexes)
        chart.render_to_png("graphs/algust2hed_regular_vs_teised.png")

    def get_upper_lower_user_series(self, posts_by_author):
        lower_users = [0 for _ in range(8)]
        upper_users = [0 for _ in range(8)]
        for author, posts_by_year in posts_by_author.items():
            for year, posts in posts_by_year.items():
                lower_count = 0
                upper_count = 0
                if len(posts) > 0:
                    for post in posts:
                        for word in post.firsts:
                            if word.istitle():
                                upper_count += 1
                            else:
                                lower_count += 1
                    if upper_count > lower_count:
                        upper_users[year - 2001] += 1
                    else:
                        lower_users[year - 2001] += 1
        return lower_users, upper_users

    # --- End capitalization graphs ---
    # --- Begin special letters graphs ---


    def graph_secial_letters(self):
        full_counter = Counter()
        for counter in self.special_letter_word_counters:
            full_counter += counter

        result = {}
        for word, _ in full_counter.most_common(5):
            for index, counter in enumerate(self.special_letter_word_counters):
                if word not in result:
                    result[word] = []
                result[word].append((counter[word] / self.post_counts_series[index]) * 100)

        chart = pygal.Line(legend_at_bottom=True, style=LightColorizedStyle)
        chart.title = "Võõrtähtedega sõnade kasutus 100 postituse kohta"
        chart.x_labels = map(str, range(2001, 2009))

        for word, series in result.items():
            chart.add(word, series)

        chart.render_to_png("graphs/v66rt2hed.png")

    # --- End special letters graphs ---

    # --- Begin lengthened word graphs ---

    def graph_lengthened_words(self):

        top_words = {}
        for counter in self.lengthened_word_counters:
            for word, count in counter.most_common(4):
                top_words[word] = []
        for counter in self.lengthened_word_counters:
            for word in top_words.keys():
                top_words[word].append(counter[word])

        for word, counts in top_words.items():
            print(word, counts)

        chart = pygal.Line(legend_at_bottom=True, style=LightColorizedStyle)
        chart.title = "Populaarseimate pikendatud sõnade kasutus aastas"
        chart.x_labels = map(str, range(2001, 2009))
        for word, series in top_words.items():
            chart.add(word, series)
        chart.render_to_png("graphs/pikendatud_enimkasutatud.png")

    def graph_lengthened_words_overall(self):
        usage_series = []
        for index, counter in enumerate(self.lengthened_word_counters):
            usage_series.append(sum(counter.values()) * 100 / self.post_counts_series[index])

        chart = pygal.Line(legend_at_bottom=True, style=LightColorizedStyle, show_legend=False)
        chart.title = "Tähekordustega sõnade kasutus 100 postituse kohta"
        chart.x_labels = map(str, range(2001, 2009))
        chart.add("", usage_series)
        chart.render_to_png("graphs/pikendatud_kasutus.png")

    # --- End lengthened word graphs ---

    # --- Begin abbreviation graphs ---

    def graph_est_abbreviations(self):

        full_counter = Counter()
        for counter in self.est_abbreviation_counters:
            full_counter += counter

        print("est lühendid")
        for abbreviation, count in full_counter.most_common(100):
            print("{}: {}".format(abbreviation, count))

        top_abbreviations = {}
        for counter in self.est_abbreviation_counters:
            for abbreviation, count in counter.most_common(3):
                top_abbreviations[abbreviation] = []
        for counter in self.est_abbreviation_counters:
            for abbreviation in top_abbreviations.keys():
                top_abbreviations[abbreviation].append(counter[abbreviation])

        for abbreviation, counts in top_abbreviations.items():
            print(abbreviation, counts)

        chart = pygal.Line(legend_at_bottom=True, style=LightColorizedStyle)
        chart.title = "Populaarseimate lühendite kasutus aastas"
        chart.x_labels = map(str, range(2001, 2009))
        for abbreviation, series in top_abbreviations.items():
            chart.add(abbreviation, series)
        chart.render_to_png("graphs/lyhendid_enimkasutatud_est.png")

    def graph_eng_abbreviations(self):

        full_counter = Counter()
        for counter in self.eng_abbreviation_counters:
            full_counter += counter

        full = {key: [0 for _ in range(8)] for key in full_counter.keys()}

        top_abbreviations = {}
        for index, counter in enumerate(self.eng_abbreviation_counters):
            for abbreviation, count in counter.most_common(3):
                top_abbreviations[abbreviation] = []
            for abbreviation, count in counter.items():
                full[abbreviation][index] = count
        for counter in self.eng_abbreviation_counters:
            for abbreviation in top_abbreviations.keys():
                top_abbreviations[abbreviation].append(counter[abbreviation])

        print("eng lühendid")
        for abbreviation, series in full.items():
            print("{}: {}".format(abbreviation, series))

        chart = pygal.Line(legend_at_bottom=True, style=LightColorizedStyle)
        chart.title = "Populaarseimate ingliskeelsete lühendite kasutus aastas"
        chart.x_labels = map(str, range(2001, 2009))
        for abbreviation, series in top_abbreviations.items():
            chart.add(abbreviation, series)
        chart.render_to_png("graphs/lyhendid_enimkasutatud_eng.png")

    # --- End abbreviation word graphs ---


    # --- Spreadsheet generation ---

    def generate_spreadsheet(self):
        wb = Workbook()
        pa1, pa2, ea1, ea2, ea3, ea4, la1, la2 = self.create_sheets(wb)

        emoticon_counts, emoticon_rows, lower_counts, post_counts, upper_counts = self.gather_data(self.posts_by_year)

        pa1.append(post_counts + [sum(post_counts)])
        ea1.append(emoticon_counts + [sum(emoticon_counts)])
        for emoticon, ea2row in emoticon_rows.items():
            ea2.append(['"' + emoticon + '"'] + ea2row + [sum(ea2row)])

        la1.append(["Suur algustäht"] + upper_counts + [sum(upper_counts)])
        la1.append(["Väike algustäht"] + lower_counts + [sum(lower_counts)])

        for author, posts_by_year in self.posts_by_author.items():
            emoticon_counts, emoticon_rows, lower_counts, post_counts, upper_counts = self.gather_data(posts_by_year)
            pa2.append([author] + post_counts + [sum(post_counts)])
            ea3.append([author] + emoticon_counts + [sum(emoticon_counts)])
            ea4.append([author])
            for emoticon, ea4row in emoticon_rows.items():
                ea4.append(['"' + emoticon + '"'] + ea4row + [sum(ea4row)])
            la2.append([author])
            la2.append(["Suur algustäht"] + upper_counts + [sum(upper_counts)])
            la2.append(["Väike algustäht"] + lower_counts + [sum(lower_counts)])

        wb.save("data.xlsx")

    def gather_data(self, posts_by_year):
        post_counts = []
        emoticon_counts = []
        upper_counts = []
        lower_counts = []
        emoticon_rows = defaultdict(list)
        for year in data.all_years:
            if year in posts_by_year:
                posts = posts_by_year[year]
            else:
                posts = []
            post_counts.append(len(posts))
            emoticons = defaultdict(int)
            emoticon_count = 0
            lower_count = 0
            upper_count = 0
            for post in posts:
                for emoticon in post.emoticons:
                    emoticons[emoticon] += 1
                    emoticon_count += 1
                for word in post.firsts:
                    if word.istitle():
                        upper_count += 1
                    else:
                        lower_count += 1
            for emoticon in data.all_emoticons:
                emoticon_rows[emoticon].append(emoticons[emoticon])
            emoticon_counts.append(emoticon_count)
            upper_counts.append(upper_count)
            lower_counts.append(lower_count)
        return emoticon_counts, emoticon_rows, lower_counts, post_counts, upper_counts

    def create_sheets(self, wb):
        pa1 = wb.active
        pa1.title = "Postitusi aastas 1"
        pa2 = wb.create_sheet(title="Postitusi aastas 2")  # kasutajate kaupa
        ea1 = wb.create_sheet(title="Emotikone aastas 1")
        ea2 = wb.create_sheet(title="Emotikone aastas 2")  # emotikonide kaupa
        ea3 = wb.create_sheet(title="Emotikone aastas 3")  # kasutajate kaupa
        ea4 = wb.create_sheet(title="Emotikone aastas 4")  # kasutajate ja emotikonide kaupa
        la1 = wb.create_sheet(title="Lause algustäht aastas 1")
        la2 = wb.create_sheet(title="Lause algustäht aastas 2")  # kasutajate kaupa
        pa1.append(data.all_years + ["kokku"])
        pa2.append([None] + data.all_years + ["kokku"])
        ea1.append(data.all_years + ["kokku"])
        ea2.append([None] + data.all_years + ["kokku"])
        ea3.append([None] + data.all_years + ["kokku"])
        ea4.append([None] + data.all_years + ["kokku"])
        la1.append([None] + data.all_years + ["kokku"])
        la2.append([None] + data.all_years + ["kokku"])
        return pa1, pa2, ea1, ea2, ea3, ea4, la1, la2


if __name__ == "__main__":

    try:
        with open("data.pickle", "rb") as f:
            data = pickle.load(f)
    except OSError:
        with open("data.pickle", "wb") as f:
            data = Data()
            pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)

    data.draw_graphs()

    # data.generate_spreadsheet(data)
